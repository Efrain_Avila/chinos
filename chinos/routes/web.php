<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Ruta de prueba
Route::get('hola', function(){
    echo "hola";
});

//Ruta de arreglo
Route::get('arreglo', function(){


    //defino un arreglo
    $estudiantes = ["AN"=>"Ana", 
                    "M"=>"Maria", 
                    "VA"=>"Valeria", 
                    "CA"=> "Carlos"];

    //ciclos foreach: recorrer arreglos
    foreach($estudiantes as $indice => $e){
        echo "<pre>$e</pre>","tiene el indice:<pre>$indice</pre>";
    }
});

//Ruta de paises
Route::get('paises', function(){
    $paises=[
        "Colombia" => [
            "Capital" => "Bogotá",
            "Moneda" => "Peso",
            "Poblacion" => 50372424 ,
            "Ciudades" => [ "Medellín", "Cali", "Barranquilla"]
        ],
        "Peru" => [
            "Capital" => "Lima",
            "Moneda" => "Sol",
            "Poblacion" => 33050325,
            "Ciudades" => [ "Cuzsco", "Trujillo" , "Arequipa"]
        ],
        "Ecuador" => [
            "Capital" => "Quito",
            "Moneda" => "Dolar",
            "Poblacion" => 17517141,
            "Ciudades" => [ "Guayaquil", "Manta","Cuenca"]
        ],  
        "Brasil " => [
            "Capital" => "Brasilia",
            "Moneda" => "Real",
            "Poblacion" => 212216052,
            "Ciudades" => [ "Rio de Janeiro", "Recife", "Bahia"]
        ]
    ];
    //Enviar datos a una vista
    //con la funcion view
    return view('paises')->with("paises" , $paises);
});