<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    </head>
    <body>
        <h1>Lista de paises </h1>
        <table class= table table-hover table-border>
            <thead>
                <th> País </th>
                <th> Capital </th>
                <th> Moneda </th>
                <th> Población </th>
                <th> Ciudades Principales </th>
            </thead>
            <tbody>
                <!--recorro a tablaforeach blade-->
                @foreach($paises as $pais => $infopais)
                <tr>
                    <td rowspan="3">{{ $pais }}</td>
                    <td rowspan="3">{{ $infopais["Capital"]}}</td>
                    <td rowspan="3">{{ $infopais["Moneda"]}}</td>
                    <td rowspan="3">{{ $infopais["Poblacion"]}}</td>
                    <td class="text-success">{{$infopais["Ciudades"][0]}}</td>
                </tr>
                <tr>
                    <th class="text-success">{{$infopais["Ciudades"][1]}}</th>
                </tr>
                <tr>
                    <th class="text-success">{{$infopais["Ciudades"][2]}}</th>
                </tr>
                @endforeach
            </tbody>
        </table>
        
        <script src="" async defer></script>
    </body>
</html>